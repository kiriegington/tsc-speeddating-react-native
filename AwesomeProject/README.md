# React-Native Cube Speed Dating App

## Before you start
Download Xcode (I'm using version 10, though the iOS guys have found some compatibility issues with 10 so are using Xcode 9.3.5)<br>
node: 10.9.0<br>
npm: 6.2.0

* cd into AwesomeProject (don't judge the name... I was following the get started guide 😂)
* run `npm install`
* run: `react-native run-ios`
* if you have issues with the above, try running: `react-native run-ios --port 8080`

### Xcode Simulator Guide:
* ctrl + cmd + z brings up a menu where you can access the debugger or inspector<br>

Note: I have encountered all sorts of crazy issues caused by the debugger which I have listed my fix for below

### Troubleshooting:

#### Simulator errors
I found i was sometimes getting weird errors in the simulator when running the debugger. E.g. an error about a typo I made 3 days previously which wasn't even in the codebase anymore - then when I disabled the debugger, everything worked fine 🤷‍<br><br>
So if your simulator is throwing all sorts of random errors, try the below steps:
* Check the build is succeeding & bundles are okay in your Terminal window
* Close all the terminal windows and simulator
* Check your node & npm versions
* Try running: `rm -rf node_modules && npm i && npm start -- --reset-cache`
* Navigate to AwesomeProject/ios and delete the build folder (this will be regenerated when you next run: `react-native run-ios`)
* If all else fails, restart your mac 🙃

### Things to think about when going from web to mobile:
* Flex doesn't work the same way as we are used to in regular CSS land
* Need to keep phone safe areas in mind (things like the dreaded iphone notch, etc)

### Future plans:
* Waiting for the users endpoint to go live, once this has been done we can swap out the hard coded cube data from app.json and hook it up with the 3SC website
* Look into testing
* Randomise the 2 truths and a lie buttons, otherwise the game is super easy
* Add restart game button
