// Actions are payloads of information that send data from your application to your store
// They are the only source of information for the store
// actions only describe what happened, but don't describe how the application's state changes.(this is what reducers are for)
// You send them to the store using dispatch

import * as types from './types';
import Api from '../lib/api';
import cubeData from '../../app.json';

export function fetchCubes() {
  return (dispatch, getState) => {
    dispatch(setStoredCubes({ cubes: cubeData.cubes }));
  }
}

export function setStoredCubes( { cubes } ) {
  return {
    type: types.SET_STORED_CUBES,
    cubes
  }
}

export function increaseQuestionNumber() {
  return {
    type: types.INCREASE_QUESTION_NUMBER,
  }
}

export function increaseScore() {
  return {
    type: types.INCREASE_SCORE,
  }
}
