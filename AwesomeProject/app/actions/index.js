// this file is for combining all the different action files that
// which will exist in larger applications

import * as CubeActions from './cubes';

export const ActionCreators = Object.assign({},
  CubeActions,
);
