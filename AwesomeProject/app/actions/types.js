// Actions must have a type property that indicates the type of action being performed.
// Types should typically be defined as string constants.
// all types are defined in this file for ease as the application grows

export const SET_STORED_CUBES = 'SET_STORED_CUBES';
export const INCREASE_QUESTION_NUMBER = 'INCREASE_QUESTION_NUMBER';
export const INCREASE_SCORE = 'INCREASE_SCORE';
