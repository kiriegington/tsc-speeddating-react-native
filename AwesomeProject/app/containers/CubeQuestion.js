import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableHighlight, ScrollView, Image, StyleSheet, Button, Alert } from 'react-native';
import { ActionCreators } from '../actions'
import { bindActionCreators } from 'redux';
import Score from './Score';

class CubeQuestion extends Component {
  constructor(props) {
    super(props);
  }

  getCubes() {
    let cubeInfo = Object.keys(this.props.storedCubes).map( key => this.props.storedCubes[key] );
    let cubesLength = cubeInfo.length;
    let questionNumber = this.props.questionNumber;

    if ((cubeInfo && cubeInfo[this.props.questionNumber]) && (questionNumber < cubesLength)) {
      return <View>
        <Image source={ {uri: cubeInfo[this.props.questionNumber].avatar} } style={{width: 350, height: 150}} />
        <Text style={{
            textAlign: 'center',
            marginTop: 20,
            marginBottom: 20,
            fontSize: 26,
            color: '#ffffff',
          }}>{cubeInfo[this.props.questionNumber].name}</Text>

        <View>
          <TouchableHighlight underlayColor="#ffffff25" onPress={ () => this.wrongAnswer() } style={styles.buttonStyle}>
            <Text style={{
                textAlign: 'center',
                color: '#ffffff',
                fontSize: 18,
                fontWeight: 'bold',
              }}>{cubeInfo[this.props.questionNumber].truth_1}</Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="#ffffff25" onPress={ () => this.wrongAnswer() } style={styles.buttonStyle}>
            <Text style={{
                textAlign: 'center',
                color: '#ffffff',
                fontSize: 18,
                fontWeight: 'bold',
              }}>{cubeInfo[this.props.questionNumber].truth_2}</Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="#ffffff25" onPress={ () => this.rightAnswer() } style={styles.buttonStyle}>
            <Text style={{
                textAlign: 'center',
                color: '#ffffff',
                fontSize: 18,
                fontWeight: 'bold',
              }}>{cubeInfo[this.props.questionNumber].lie}</Text>
          </TouchableHighlight>
        </View>
      </View>
    } else {
      return <View>
      </View>
    }
  }

  skipLink() {
    let cubeInfo = Object.keys(this.props.storedCubes).map( key => this.props.storedCubes[key] );
    let cubesLength = cubeInfo.length;
    let questionNumber = this.props.questionNumber;

    if ((cubeInfo && cubeInfo[this.props.questionNumber]) && (questionNumber < cubesLength)) {
      return <View style={{ alignSelf: 'center' }}>
        <TouchableHighlight underlayColor="#ffffff00" onPress={ () => this.skipCube() } style={styles.skipLinkStyle}>
          <Text style={{
              textAlign: 'center',
              color: '#ffffff',
              fontSize: 18,
            }}>Skip this cube?</Text>
        </TouchableHighlight>
      </View>
    }
  }

  rightAnswer() {
    this.props.increaseScore();
    this.props.increaseQuestionNumber();
  }

  wrongAnswer() {
    this.props.increaseQuestionNumber();
  }

  skipCube() {
    Alert.alert(
      'Yo - do you really want to skip?',
      'they\'ll be removed from the game, so you should only do this is they are out today, or if they are imaginary',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Skip', onPress: () => this.props.increaseQuestionNumber()},
      ],
      { cancelable: false }
    )
  }

  render() {
    return (
      <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
      <Score />
      {this.getCubes()}
      {this.skipLink()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    marginBottom: 10,
    textAlign: 'center',
  },
  imgStyle: {
    marginBottom: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  buttonStyle: {
    marginTop: 10,
    padding: 10,
    borderColor: '#ffffff',
    borderWidth: 3,
    backgroundColor: '#adec39',
  },
  skipLinkStyle: {
    marginTop: 30,
    marginBottom: 30,
    paddingBottom: 5,
    borderBottomColor: '#ffffff',
    borderBottomWidth: 1,
    alignSelf: 'flex-start',
  }
});

function mapStateToProps(state) {
  return {
    storedCubes: state.storedCubes,
    questionNumber: state.questionNumber,
    increaseScore: state.increaseScore,
    currentScore: state.currentScore,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CubeQuestion);
