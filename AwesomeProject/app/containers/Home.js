import React, { Component } from 'react';
import {
  ScrollView,
  View,
  TextInput,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import Score from './Score';
import CubeQuestion from './CubeQuestion';

class Home extends Component {
  searchPressed() {
      this.props.fetchCubes();
  }

  cubeList() {
    return Object.keys(this.props.storedCubes).map( key => this.props.storedCubes[key] )
  }

  showStartScreen() {
    let cubesLength = this.cubeList().length;
    let questionNumber = this.props.questionNumber - 1;
    if (cubesLength > 0 && (questionNumber < cubesLength)) {
      return <CubeQuestion />
    } else {
      return <View style={{
        flex: 1,
        color: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
        <TouchableHighlight underlayColor="#ffffff00" onPress={ () => this.searchPressed() } style={{
          flex: 1,
          backgroundColor: '#000000',
          justifyContent: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
          backgroundColor: '#adec39',
        }}>
          <Text style={{
              flex: 1,
              textAlign: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              color: '#ffffff',
              backgroundColor: '#adec39',
              width: '100%',
              fontSize: 28,
            }}>Tap to start</Text>
        </TouchableHighlight>
        </View>
    }
  }

  render() {
    return <View style={{
        flex: 1,
        paddingTop: 30,
        paddingBottom: 30,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#adec39'
      }}>
      {this.showStartScreen()}
    </View>
  }
}

function mapStateToProps(state) {
  return {
    storedCubes: state.storedCubes,
    questionNumber: state.questionNumber,
  }
}

export default connect(mapStateToProps)(Home)
