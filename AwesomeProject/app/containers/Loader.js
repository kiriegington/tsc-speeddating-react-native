import React, { Component } from 'react';
import { Text, View } from 'react-native';

export class Loader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <Text>Loading Cubes</Text>
      </View>
    );
  }
}

export default Loader;
