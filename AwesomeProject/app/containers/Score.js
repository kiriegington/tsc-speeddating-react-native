import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';

class Score extends Component {
  constructor(props) {
    super(props)
  }

  cubes() {
    return Object.keys(this.props.storedCubes).map( key => this.props.storedCubes[key] )
  }

  showScore() {
    let cubeInfo = Object.keys(this.props.storedCubes).map( key => this.props.storedCubes[key] );
    let cubesLength = cubeInfo.length;
    let questionNumber = this.props.questionNumber;

    if (questionNumber < cubesLength) {
      return <Text style={{
          textAlign: 'center',
          color: '#ffffff',
          fontSize: 18,
          paddingTop: 20,
        }}>
        SCORE: {this.props.currentScore} / {this.cubes().length}
      </Text>
    } else {
      return <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <Text style={{
            textAlign: 'center',
            color: '#ffffff',
            fontSize: 18,
          }}>{`Game over!\nYou guessed ${this.props.currentScore} out of ${this.cubes().length} lies correctly`}</Text>
      </View>
    }
  }

  render() {

    return (
      <View style={{
          textAlign: 'center',
          marginTop: 20,
          marginBottom: 20,
        }}>
      {this.showScore()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    storedCubes: state.storedCubes,
    questionNumber: state.questionNumber,
    currentScore: state.currentScore,
  }
}

export default connect(mapStateToProps)(Score)
