// Reducers specify how the application's state changes in response to actions sent to the store.
// actions only describe what happened, but don't describe how the application's state changes.

import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const storedCubes = createReducer({}, {
  [types.SET_STORED_CUBES](state, action) {
    let newState = [];
    action.cubes.forEach( (cube) => {
      newState[cube.name] = cube
    });
    return newState;
  }
});

export const questionNumber = createReducer(0, {
  [types.INCREASE_QUESTION_NUMBER](state, action) {
    return state + 1
  }
});

export const currentScore = createReducer(0, {
  [types.INCREASE_SCORE](state, action) {
    return state + 1
  }
});
