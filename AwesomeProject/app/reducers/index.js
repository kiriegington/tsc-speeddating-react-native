// you could have many different reducers
// this file combines them all to make development easier

import { combineReducers } from 'redux';
import * as cubeReducer from './cubes';

export default combineReducers(Object.assign(
  cubeReducer,
))
